import React, { Fragment, Component } from 'react'
import { Link } from 'react-router-dom'
import TextField from '../../components/input';

import './profile.css'

export default class Profile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            genres: ['Комедии', 'Семейные', 'Триллеры', 'Детские', 'Мультфильмы', 'Военные', 'Детективы', 'Боевики',
                'Криминальные', 'Мелодрамы', 'Сериалы', 'Ужасы', 'Приключения', 'Исторические', 'Фантастика', 'Биография', 'Мюзиклы', 'Драмы'],
            selected: {},
            changeProfile: 1
        };

        this.selectGenre = this.selectGenre.bind(this);
        this.renderGenre = this.renderGenre.bind(this);
    }

    // changeStep(type) {
    //     this.setState(prevState => {
    //         return { count: type == 'add' ? prevState.count + 1 : prevState.count - 1 }
    //     });
    // }

    changeProfile(i) {
        this.setState({ changeProfile: i });
    }


    selectGenre(el) {
        var selected = this.state.selected;
        selected[el] = !selected[el];
        this.setState({ selected: selected });
    }

    renderGenre(el) {
        var className = this.state.selected[el] ? 'sign-genre-selected' : '';
        var onClick = this.selectGenre.bind(this, el);
        return <span className={`sign-genre ${className}`} onClick={onClick} key={el}>{el}</span>;
    }

    render() {
        return (<Fragment>
            <div className="user-body">
                <div className="user-info">
                    <div className="sign-title">Мой профиль</div>
                    <div className="menu-sidebar-profile">
                        <div className="menu-sidebar-avatar">
                            <img src="images/persons/mamoa.jpeg" alt="" />
                        </div>
                        <div className="menu-sidebar-profile-body">
                            <div className="menu-sidebar-profile-name">Нариман Дуйсеков</div>
                            <div className="menu-sidebar-profile-contacts">+7 700 200 50 10</div>
                            <div className="menu-sidebar-profile-contacts">nariman.duisekov@gmail.com</div>
                        </div>
                    </div>
                    {this.state.changeProfile === 1 ? <div className="menu-m-profile">
                        <div className="menu-m-profile-item">
                            <div className="menu-m-profile-label">Почта</div>
                            <div className="menu-m-profile-text">nariman.duisekov@gmail.com</div>
                        </div>
                        <div className="menu-m-profile-item">
                            <div className="menu-m-profile-label">Номер телефона</div>
                            <div className="menu-m-profile-text">+77002005010</div>
                        </div>
                        <div className="sign-button" onClick={() => this.changeProfile(2)}>
                            Изменить
                        </div>
                        <div className="menu-m-profile-exit">
                            Выйти
                        </div>
                    </div> : null}
                    {this.state.changeProfile === 2 ? <div className="menu-m-profile">
                        <TextField value="Нариман"
                            text="Изменить имя" width="100%"
                        />
                        <TextField value="Дуйсеков"
                            text="Изменить фамилию" width="100%"
                        />
                        <TextField value="+7747744777"
                            text="Изменить телефон" width="100%"
                        />
                        <TextField value="nariman.duisekov@gmail.com"
                            text="Изменить почту" width="100%"
                        />
                        <div className="sign-button" onClick={() => this.changeProfile(1)}>
                            Сохранить
                        </div>
                        {/* <div className="menu-m-profile-exit" onClick={() => this.changeProfile(1)}>
                            Назад
                        </div> */}
                    </div> : null}
                </div>
                <div className="user-genres">
                    <div className="sign-title">Подборка для Вас</div>
                    <div className="sign-subtitle">Выберите, как минимум, три жанра.</div>
                    <div className="sign-genres">
                        {
                            this.state.genres.map(this.renderGenre)
                        }
                    </div>
                    {Object.keys(this.state.selected).length < 3 ? <div className="sign-button sign-button-dasable">
                        Сохранить
                    </div> : <div className="sign-button">
                            Сохранить
                    </div>}
                </div>
            </div>
        </Fragment>
        )
    }
}