import React, { Fragment, Component } from 'react'
import { Link } from 'react-router-dom'

import axios from 'axios';

import "./friends.css"
// import films from "../main/recomend.json"
// import MyFilmItem from "../../components/my-film-item"


export default class Favourite extends Component {
    constructor(props) {
        super(props);
        this.state = {
            friends: [
                { id: 1, name: 'Нариман Дуйсеков', films: ['Брат или брак', 'Брат или брак 2', 'Бизнес по-казахски'] },
                { id: 2, name: 'Еламан Мырзаханов', films: ['Рэкетир', 'Бизнесмены', 'Районы', 'Тараз'] },
                { id: 3, name: 'Нариман Дуйсеков', films: ['Брат или брак', 'Брат или брак 2', 'Бизнес по-казахски'] },
                { id: 4, name: 'Еламан Мырзаханов', films: ['Рэкетир', 'Бизнесмены', 'Районы', 'Тараз'] },
                { id: 5, name: 'Нариман Дуйсеков', films: ['Брат или брак', 'Брат или брак 2', 'Бизнес по-казахски'] },
                { id: 6, name: 'Еламан Мырзаханов', films: ['Рэкетир', 'Бизнесмены', 'Районы', 'Тараз'] },
                { id: 7, name: 'Нариман Дуйсеков', films: ['Брат или брак', 'Брат или брак 2', 'Бизнес по-казахски'] },
                { id: 8, name: 'Еламан Мырзаханов', films: ['Рэкетир', 'Бизнесмены', 'Районы', 'Тараз'] },
                { id: 9, name: 'Нариман Дуйсеков', films: ['Брат или брак', 'Брат или брак 2', 'Бизнес по-казахски'] },
                { id: 10, name: 'Еламан Мырзаханов', films: ['Рэкетир', 'Бизнесмены', 'Районы', 'Тараз'] },
                { id: 11, name: 'Нариман Дуйсеков', films: ['Брат или брак', 'Брат или брак 2', 'Бизнес по-казахски'] },
                { id: 12, name: 'Еламан Мырзаханов', films: ['Рэкетир', 'Бизнесмены', 'Районы', 'Тараз'] },
            ]
        };
    }

    componentDidMount() {
        window.scrollTo(0, 0)
    }

    render() {
        return (
            <Fragment>
                <div className="friends">
                    <div className="friends-main">
                        <div className="container">
                            <div className="page-title">Друзья</div>
                            <div className="friends-body">
                                {
                                    this.state.friends.map(function (friend) {
                                        return <Link to="/friend" key={friend.id} className="friends-item">
                                            <span className="friends-img">
                                                <img src="/images/persons/mamoa.jpeg" alt="" />
                                            </span>
                                            <span className="friends-info">
                                                <div className="friends-name">{friend.name}</div>
                                                <div className="friends-films">
                                                    {
                                                        friend.films.map(function (film) {
                                                            return <span key={film}>{film},</span>
                                                        })
                                                    }
                                                </div>
                                            </span>
                                        </Link>
                                    })
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        )
    }
}

