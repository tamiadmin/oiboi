import React, { Fragment, Component } from 'react'
import { Link } from 'react-router-dom'
import TextField from '../../components/input';

import axios from 'axios';
import $ from 'jquery';

import './results.css'

import FilmItem from "../../components/film-item"
import CastItem from "../../components/cast-item"

export default class Profile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            movies: [],
            images: [],
            cast: [],
            buttonsActive: 1
        };

        // this.selectGenre = this.selectGenre.bind(this);
        // this.renderGenre = this.renderGenre.bind(this);
    }

    componentWillMount() {
        axios.get('https://api.themoviedb.org/3/person/3223/movie_credits?api_key=d09aed91f13503607360b4e7fcbf8140')
            .then((response) => {
                this.setState({
                    ...this.state,
                    movies: response.data.cast
                })
            })
            .catch((err) => {
                console.log(err.response)
            })

        axios.get('https://api.themoviedb.org/3/person/3223/images?api_key=d09aed91f13503607360b4e7fcbf8140')
            .then((response) => {
                this.setState({
                    ...this.state,
                    images: response.data.profiles
                })
            })
            .catch((err) => {
                console.log(err.response)
            })
        axios.get('https://api.themoviedb.org/3/movie/458156/credits?api_key=d09aed91f13503607360b4e7fcbf8140&language=ru&include_image_language=ru,null')
            .then((response) => {
                this.setState({
                    ...this.state,
                    cast: response.data.cast
                })
                console.log(this.state.cast)
            })
            .catch((err) => {
                console.log(err.response)
            })
    }
    // changeStep(type) {
    //     this.setState(prevState => {
    //         return { count: type == 'add' ? prevState.count + 1 : prevState.count - 1 }
    //     });
    // }

    changeProfile(i) {
        this.setState({ changeProfile: i });
    }

    // selectGenre(el) {
    //     var selected = this.state.selected;
    //     selected[el] = !selected[el];
    //     this.setState({ selected: selected });
    // }

    // renderGenre(el) {
    //     var className = this.state.selected[el] ? 'sign-genre-selected' : '';
    //     var onClick = this.selectGenre.bind(this, el);
    //     return <span className={`sign-genre ${className}`} onClick={onClick} key={el}>{el}</span>;
    // }

    render() {
        console.log(this.state.cast)
        return (
            <Fragment>
                <div className="results">
                    <div className="container">
                        <div className="page-title results-title">
                            <input type="text" className="results-input" placeholder="Поиск по сайту" />
                        </div>
                        <div className="results-buttons">
                            {this.state.buttonsActive == 1 ? <span className="result-buttons-active">Друзья</span> : <span onClick={() => this.setState({ buttonsActive: 1 })}>Друзья</span>}
                            {this.state.buttonsActive == 2 ? <span className="result-buttons-active">Фильмы</span> : <span onClick={() => this.setState({ buttonsActive: 2 })}>Фильмы</span>}
                            {this.state.buttonsActive == 3 ? <span className="result-buttons-active">Актёры</span> : <span onClick={() => this.setState({ buttonsActive: 3 })}>Актёры</span>}
                            {this.state.buttonsActive == 4 ? <span className="result-buttons-active">Режиссёры</span> : <span onClick={() => this.setState({ buttonsActive: 4 })}>Режиссёры</span>}
                            {this.state.buttonsActive == 5 ? <span className="result-buttons-active">Продюсеры</span> : <span onClick={() => this.setState({ buttonsActive: 5 })}>Продюсеры</span>}
                            {this.state.buttonsActive == 6 ? <span className="result-buttons-active">Сценаристы</span> : <span onClick={() => this.setState({ buttonsActive: 6 })}>Сценаристы</span>}
                        </div>
                        {this.state.buttonsActive == 1 ?
                            <div className="cast-movies">
                                <div className="cast-movies">
                                    {
                                        this.state.movies.map(function (film) {
                                            return <FilmItem key={film.id} film={film} />
                                        })
                                    }
                                </div>
                            </div> : null}
                        {this.state.buttonsActive == 3 ?
                            <div className="cast-movies">
                                <div className="cast-movies">
                                    {
                                        this.state.cast.map(function (item,key) {
                                            return <CastItem key={key} cast={item} />
                                        })
                                    }
                                </div>
                            </div> : null}

                    </div>
                </div>
            </Fragment>
        )
    }
}