import React, { Component } from 'react';
import Hls from 'hls.js';
import Plyr from 'react-plyr'

export default class PlyrPlayer extends Component {
  constructor(props, context) {
    super(props, context);
    this.hls = new Hls();
  }

  componentDidMount() {
    // `src` is the property get from this component
    // `video` is the property insert from `Video` component
    // `video` is the html5 video element
    var video = document.querySelector('video');

    if (Hls.isSupported()) {
      var hls = new Hls();
      hls.loadSource(this.props.src);
      hls.attachMedia(video);
      hls.on(Hls.Events.MANIFEST_PARSED, function () {
        video.play();
      });
    }
  }

    componentWillUnmount() {
      // destroy hls video source
      if (this.hls) {
        this.hls.destroy();
      }
    }

    render() {
      return (
        <Plyr type="video" autoPlay="true" className="video-hls-my" />
      );
    }
  }