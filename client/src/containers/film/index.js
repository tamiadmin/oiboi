import React, { Fragment, Component } from 'react'
import { Link, Redirect } from 'react-router-dom'
import axios from 'axios'
import PlyrPlayer from './HLSSource';
import "./film.css"

import Carousel from "../../components/carousel"


export default class Film extends Component {
    constructor(props) {
        super(props);
        this.state = {
            moreinfo: 'moreinfo',
            showTrailer: false,
            films: [],
            film: {},
            film_id: props.match.params.id,
            cast: []
        };
        this.pay = this.pay.bind(this);
    }

    componentWillMount() {

        axios.get('https://api.themoviedb.org/3/movie/' + this.state.film_id + '?api_key=d09aed91f13503607360b4e7fcbf8140&language=ru&include_image_language=ru,null')
            .then((response) => {
                this.setState({
                    ...this.state,
                    film: response.data
                })
            })
            .catch((err) => {
                console.log("Fail")
            })

        axios.get('https://api.themoviedb.org/3/movie/' + this.state.film_id + '/similar?api_key=d09aed91f13503607360b4e7fcbf8140&language=ru&include_image_language=ru,null')
            .then((response) => {
                this.setState({
                    ...this.state,
                    films: response.data.results
                })
            })
            .catch((err) => {
                console.log(err.response)
            })

        axios.get('https://api.themoviedb.org/3/movie/' + this.state.film_id + '/credits?api_key=d09aed91f13503607360b4e7fcbf8140&language=ru&include_image_language=ru,null')
            .then((response) => {
                this.setState({
                    ...this.state,
                    cast: response.data.cast
                })
            })
            .catch((err) => {
                console.log(err.response)
            })
    }

    pay() {
        var widget = new cp.CloudPayments();
        widget.charge({
            publicId: 'pk_2765d3e92d184877effe59ff9082b', //id из личного кабинета
            description: 'Пример оплаты (деньги сниматься не будут)', //назначение
            amount: 1,
            currency: 'KZT',
            invoiceId: '1234567',
            accountId: 'takhirmunarbekov@gmail.com',
            skin: "modern",
            data: {
                myProp: 'myProp value'
            }
        },
            function (options) { // success
                //действие при успешной оплате
            },
            function (reason, options) { // fail
                //действие при неуспешной оплате
            });
    };

    componentDidMount() {
        window.scrollTo(0, 0);
        document.title = this.state.film.title;
    }

    render() {
        return (
            <Fragment>
                <div className="film">
                    <div className="player" style={{ backgroundImage: `url(https://image.tmdb.org/t/p/original${this.state.film.backdrop_path})` }}>
                        <div className="trailer">
                            {this.state.showTrailer ?
                                <PlyrPlayer src="http://s4.seplay.net/content/stream/films/game.of.thrones.s08e02.1080p.rus.lostfilm.tv_98300/hls/1080/index.m3u8" /> :
                                <div className="trailer-play" onClick={() => this.setState({ showTrailer: true })}>
                                    <img className="trailer-play-button" src="images/buttons/play.svg" alt="" />
                                </div>
                            }
                        </div>
                    </div>

                    <img src="/images/shadow.png" alt="" className="slider-shadow" />
                    <div className="f-main-section">
                        <div className="container">
                            <div className="f-i-body">
                                <div className="f-left">
                                    <div className="f-poster">
                                        <img src={`https://image.tmdb.org/t/p/w780${this.state.film.poster_path}`} alt="" />
                                    </div>
                                    <div className="f-m-buy f-m-buy-desktop" onClick={this.pay}>
                                        <span>НАПРОКАТ: 390 Т HD</span>
                                    </div>
                                </div>
                                <div className="f-right">
                                    <div className="f-main-mob">
                                        <div className="f-poster">
                                            <img src={`https://image.tmdb.org/t/p/w780${this.state.film.poster_path}`} alt="" /></div>
                                        <div className="f-m-info">
                                            <div className="f-name">{this.state.film.title}</div><div className="f-info">2019 • 120 • Казахстан</div>
                                            <div className="f-genres"><span>Драма • Комедия</span></div>
                                            <div className="f-buttons">
                                                <span><img src="/images/buttons/age.svg" alt="" /></span>
                                                <span><img src="/images/buttons/check-disable.svg" alt="" /></span>
                                                <span><img src="/images/buttons/like-disable.svg" alt="" /></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="f-main">
                                        <div className="f-m-info">
                                            <div className="f-name">{this.state.film.title}</div>
                                            <div className="f-info">
                                                <div className="f-info-item">
                                                    <span className="f-info-name">Премьера</span><span className="f-info-content">26 ноября 2018</span>
                                                </div>
                                                <div className="f-info-item">
                                                    <span className="f-info-name">Длительность</span><span className="f-info-content">142 мин</span>
                                                </div>
                                                <div className="f-info-item">
                                                    <span className="f-info-name">Производство</span><span className="f-info-content">США, Австралия</span>
                                                </div>
                                                <div className="f-info-item">
                                                    <span className="f-info-name">Жанр</span><span className="f-info-content">Фэнтези, боевик, приключения, фантастика</span>
                                                </div>
                                                <div className="f-info-item">
                                                    <span className="f-info-name">Режиссёр</span><span className="f-info-content">Джеймс Ван</span>
                                                </div>
                                                <div className="f-info-item">
                                                    <span className="f-info-name">Продюсер</span><span className="f-info-content">Хадиджа Алами, Джон Берг, Роб Кауэн</span>
                                                </div>
                                                <div className="f-info-item">
                                                    <span className="f-info-name">Сценарий</span><span className="f-info-content">Уилл Билл, Дэвид Лесли Джонсон, Морт Вейзингер</span>
                                                </div>
                                            </div>

                                            <div className="f-buttons">
                                                <span><img src="/images/buttons/age.svg" alt="" /></span>
                                                <span><img src="/images/buttons/check-disable.svg" alt="" /></span>
                                                <span><img src="/images/buttons/like-disable.svg" alt="" /></span>
                                            </div>
                                        </div>
                                        {
                                            this.state.films.length > 0 ?
                                                <div className="f-m-gallery">
                                                    <div className="f-g-photos">
                                                        <div className="f-list s-list" data-tabindex="-1" data-uk-slider>
                                                            <div className="f-g-title"><span>Фотогалерея</span><div className="f-g-arrows"><a className="g-arrow-left" data-uk-slidenav-previous data-uk-slider-item="previous"><img src="images/buttons/arrow-left.svg" alt="" /></a><a className="g-arrow-right" data-uk-slidenav-next data-uk-slider-item="next"><img src="images/buttons/arrow-right.svg" alt="" /></a></div></div>
                                                            <ul className="uk-slider-items" data-uk-lightbox="animation: slide">
                                                                {
                                                                    this.state.films.map(function (film, key) {
                                                                        return <a className="f-g-img" key={key} href={`https://image.tmdb.org/t/p/w780${film.backdrop_path}`} data-caption={key + 1 + ' из 20'}>
                                                                            <img key={film.id} src={`https://image.tmdb.org/t/p/w780${film.backdrop_path}`} alt="" />
                                                                        </a>
                                                                    })
                                                                }
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div> : null}

                                    </div>
                                    <div className={`f-main-info ${this.state.moreinfo}`} >
                                        <div className="f-text f-script">
                                            <div className="f-t-body">
                                                {this.state.film.overview}
                                            </div>
                                        </div>
                                        <div className="f-info-mob">
                                            <div className="f-info-item">
                                                <span className="f-info-name">Режиссёр</span><Link className="f-info-content" to={{
                                                    pathname: '/person/2127'
                                                }}>Джеймс Ван</Link>
                                            </div>
                                            <div className="f-info-item">
                                                <span className="f-info-name">Продюсер</span><span className="f-info-content">Хадиджа Алами, Джон Берг, Роб Кауэн</span>
                                            </div>
                                            <div className="f-info-item">
                                                <span className="f-info-name">Сценарий</span><span className="f-info-content">Уилл Билл, Дэвид Лесли Джонсон, Морт Вейзингер</span>
                                            </div>
                                        </div>
                                        <div className="f-text">
                                            <div className="f-t-title">Интересные факты</div>
                                            <div className="f-t-body">
                                                {/* {this.state.film.facts} */}
                                                Одно время компания Леонардо ДиКаприо Appian Way Productions продюсировала фильм.
                                            </div>
                                        </div>
                                        {/* <div className="f-m-gallery-mob">
                                            <div className="f-g-photos">
                                                <div className="f-list s-list" data-tabindex="-1" data-uk-slider>
                                                    <div className="f-g-title"><span>Фотогалерея</span><div className="f-g-arrows"><a className="g-arrow-left" data-uk-slidenav-previous data-uk-slider-item="previous"><img src="images/buttons/arrow-left.svg" alt="" /></a><a className="g-arrow-right" data-uk-slidenav-next data-uk-slider-item="next"><img src="images/buttons/arrow-right.svg" alt="" /></a></div></div>
                                                    <ul className="uk-slider-items">
                                                        {
                                                            this.state.films.map(function (film) {
                                                                return <div className="f-g-img">
                                                                    <img key={film.id} src={`https://image.tmdb.org/t/p/w780${film.backdrop_path}`} alt="" />
                                                                </div>
                                                            })
                                                        }
                                                    </ul>
                                                </div>
                                            </div>
                                        </div> */}
                                    </div>
                                    {
                                        this.state.moreinfo === 'moreinfo' ? <div className="f-t-more" onClick={() => this.setState({ moreinfo: '' })}>Ещё</div> : <div className="f-t-more" onClick={() => this.setState({ moreinfo: 'moreinfo' })}>Скрыть</div>
                                    }
                                    <div className="f-m-buy f-m-buy-mobile" onClick={this.pay}>
                                        <span>НАПРОКАТ: 390 Т HD</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="similar">
                        <div className="container"><div className="similar-title f-list-title">В ролях</div></div>
                        <div className="s-slider">

                            {Object.keys(this.state.cast).length < 11 ?
                                <div className="f-list s-list" data-tabindex="-1" data-uk-slider>
                                    <div className="s-shadow"></div>
                                    <a className="s-arrow-left" data-uk-slidenav-previous data-uk-slider-item="previous"><img src="images/buttons/arrow-left.svg" alt="" /></a>
                                    <a className="s-arrow-right" data-uk-slidenav-next data-uk-slider-item="next"><img src="images/buttons/arrow-right.svg" alt="" /></a>
                                    <ul className="uk-slider-items f-items s-s-items">
                                        {
                                            this.state.cast.map(function (cast, key) {
                                                return <Link to={{
                                                    pathname: '/person/' + cast.id,
                                                    state: { cast_id: cast.id }
                                                }} className="f-item" key={key}>
                                                    <div className="f-img">
                                                        {
                                                            cast.profile_path === null ? <img src={'images/custom/actor.jpg'} alt="" /> :
                                                                <img src={`https://image.tmdb.org/t/p/w780${cast.profile_path}`} alt="" />
                                                        }
                                                    </div>
                                                    <div className="film-name">{cast.name}</div>
                                                    <div className="film-info">{cast.character}</div>
                                                </Link>
                                            })
                                        }
                                    </ul>
                                </div> : <div className="f-list s-list" data-tabindex="-1" data-uk-slider="center:true">
                                    <div className="s-shadow"></div>
                                    <a className="s-arrow-left" data-uk-slidenav-previous data-uk-slider-item="previous"><img src="images/buttons/arrow-left.svg" alt="" /></a>
                                    <a className="s-arrow-right" data-uk-slidenav-next data-uk-slider-item="next"><img src="images/buttons/arrow-right.svg" alt="" /></a>
                                    <ul className="uk-slider-items f-items">
                                        {
                                            this.state.cast.map(function (cast, key) {
                                                return <Link to={{
                                                    pathname: '/person/' + cast.id,
                                                    state: { cast_id: cast.id }
                                                }} className="f-item" key={key}>
                                                    <div className="f-img">
                                                        {
                                                            cast.profile_path === null ? <img src={'images/custom/actor.jpg'} alt="" /> :
                                                                <img src={`https://image.tmdb.org/t/p/w780${cast.profile_path}`} alt="" />
                                                        }
                                                    </div>
                                                    <div className="film-name">{cast.name}</div>
                                                    <div className="film-info">{cast.character}</div>
                                                </Link>
                                            })
                                        }
                                    </ul>
                                </div>}
                        </div>
                    </div>
                    <div className="p-similar">
                        <div className="container">
                            <div className="f-m-title">В ролях</div>
                        </div>
                        <div className="f-p-slider-cast">
                            {
                                this.state.cast.map(function (cast, key) {
                                    return <Link to={{
                                        pathname: '/person/' + cast.id,
                                        state: { cast_id: cast.id }
                                    }} key={key} className="f-actors-mob">
                                        {
                                            cast.profile_path === null ? <img src={'images/custom/actor.jpg'} alt="" /> :
                                                <img src={`https://image.tmdb.org/t/p/w500${cast.profile_path}`} alt="" />
                                        }
                                        <div className="f-actors-mob-name">{cast.name}</div>
                                        <div className="f-actors-mob-char">{cast.character}</div>
                                    </Link>
                                })
                            }
                        </div>
                    </div>

                    {
                        this.state.films.length > 0 ?
                            <Fragment>
                                <Carousel title="Похожие фильмы" items={this.state.films} />

                                {/* ?mobile */}
                                <div className="p-similar">
                                    <div className="container">
                                        <div className="f-m-title">Фотогалерея</div>
                                    </div>
                                    <div className="f-p-slider f-p-slider-images" data-uk-lightbox="animation: slide">
                                        {
                                            this.state.films.map(function (film, key) {
                                                return <a className="f-g-img" key={key} href={`https://image.tmdb.org/t/p/w780${film.backdrop_path}`} data-caption={key + 1 + ' из 20'}>
                                                <img src={`https://image.tmdb.org/t/p/w500${film.backdrop_path}`} alt="" />
                                            </a>
                                            })
                                        }
                                    </div>
                                </div>
                                <div className="p-similar">
                                    <div className="container">
                                        <div className="f-m-title">Похожие фильмы</div>
                                    </div>
                                    <div className="f-p-slider">
                                        {
                                            this.state.films.map(function (film, key) {
                                                return <img key={key} src={`https://image.tmdb.org/t/p/w780${film.poster_path}`} alt="" />
                                            })
                                        }
                                    </div>
                                </div>
                                {/* end mobile */}
                            </Fragment>
                            : null
                    }
                    <div className="f-more">
                        <div className="container">
                            <div className="f-m-title">Дополнительная информация</div>
                            <div className="f-m-body">
                                <div className="f-m-section">
                                    <div className="f-m-name">Звуковая дорожка</div>
                                    <div className="f-mm-info">Русский (стерео 5.1)</div>
                                </div>
                                <div className="f-m-section">
                                    <div className="f-m-name">Cубтитры</div>
                                    <div className="f-mm-info">Казахский</div>
                                </div>
                                <div className="f-m-section">
                                    <div className="f-m-name">Воспроизведение</div>
                                    <div className="f-mm-info">Вы можете воспроизводить видео на разных устройствах, таких как телевизор, ноутбук, планшет, телефон.</div>
                                </div>
                                <div className="f-m-section">
                                    <div className="f-m-name">Срок проката</div>
                                    <div className="f-mm-info">В течение 30 дней со дня покупки или 48 часов после начала просмотра фильма.</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment >
        )
    }
}

