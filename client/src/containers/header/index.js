import React, { Fragment, Component } from 'react'
import { Link } from 'react-router-dom'
import $ from 'jquery'
import './header.css'

import Sign from '../../containers/sign'
import Profile from '../../containers/profile'
import Results from '../../containers/results'
import TextField from '../../components/input'


export default class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {
            show: false,
            active: 1,
            showSearch: 1,
            menuSideBar: 1,
            showProfile: false,
            genres: ['Комедии', 'Семейные', 'Триллеры', 'Детские', 'Мультфильмы', 'Военные', 'Детективы', 'Боевики',
                'Криминальные', 'Мелодрамы', 'Сериалы', 'Ужасы', 'Приключения', 'Исторические', 'Фантастика', 'Биография', 'Мюзиклы', 'Драмы'],
            selected: {},
            sideBarActive: '',
            toSearch: ''
        };

        this.selectGenre = this.selectGenre.bind(this);
        this.renderGenre = this.renderGenre.bind(this);

        // this.showMenuModal = this.showMenuModal.bind(this);
        this.closeSideBar = this.closeSideBar.bind(this);
        this.openSideBar = this.openSideBar.bind(this);
        this.changeSideBar = this.changeSideBar.bind(this);
        this.openSign = this.openSign.bind(this);
        this.openProfile = this.openProfile.bind(this);
        this.closeProfile = this.closeProfile.bind(this);
        this.closeSign = this.closeSign.bind(this);
        this.handleResults = this.handleResults.bind(this);
    }


    closeSideBar(e, to) {
        e.preventDefault();
        $('body').css('overflow', 'visible');
        this.props.history.push(to);
    }

    openSideBar() {
        $('body').css('overflow', 'hidden');
    }

    closeSign() {
        this.setState({ show: false });
        $('body').css('overflow', 'visible');
    }

    openSign() {
        this.setState({ show: true });
        $('body').css('overflow', 'hidden');
    }

    closeProfile() {
        this.setState({ showProfile: false });
        $('body').css('overflow', 'visible');
    }

    openProfile() {
        this.setState({ showProfile: true });
        $('body').css('overflow', 'hidden');
    }

    changeSideBar(i) {
        this.setState({ menuSideBar: i });
        if (i == 1) {
            this.setState({ sideBarActive: '' })
        } else {
            this.setState({ sideBarActive: 'menu-sidebar-genres' })
        }
    }

    selectGenre(el) {
        var selected = this.state.selected;
        selected[el] = !selected[el];
        this.setState({ selected: selected });
    }

    renderGenre(el) {
        var className = this.state.selected[el] ? 'sign-genre-selected' : '';
        var onClick = this.selectGenre.bind(this, el);
        return <span className={`sign-genre menu-sidebar-genre ${className}`} onClick={onClick} key={el}>{el}</span>;
    }

    handleResults(e) {
        this.setState({ toSearch: e.target.value });
        if (e.target.value.length > 0) {
            $('body').css('overflow', 'hidden');
        } else {
            $('body').css('overflow', 'visible');
        }
    }

    render() {
        return (
            <Fragment>
                <div className="header">
                    <div className="container">
                        <div className="header-body">
                            {this.state.showSearch === 1 ? <Link className="logo" to="/"><img src="/images/logo.svg" alt="" /></Link> : <Link className="logo logo-mob" to="/"><img src="/images/logo.svg" alt="" /></Link>}
                            <div className="menu">
                                {
                                    this.state.showSearch === 1 ? <div className="menu-items">
                                        {this.props.location.pathname === '/' ? <Link to="/"><span className="menu-item menu-item-active">Главная</span></Link> : <Link to="/"><span className="menu-item">Главная</span></Link>}
                                        {this.props.location.pathname === '/my' ? <Link to="/my"><span className="menu-item menu-item-active">Мои фильмы</span></Link> : <Link to="/my"><span className="menu-item">Мои фильмы</span></Link>}
                                        {this.props.location.pathname === '/favorites' ? <Link to="/favorites"><span className="menu-item menu-item-active">Отмеченные</span></Link> : <Link to="/favorites"><span className="menu-item">Отмеченные</span></Link>}
                                        {/* <Link to="/my"><span className="menu-item">Мои фильмы</span></Link> */}
                                        {/* <Link to="/favorites"><span className="menu-item">Отмеченные</span></Link> */}
                                        {/* <Link to="/friends"><span className="menu-item">Друзья</span></Link> */}
                                    </div> :
                                        <Fragment>
                                            <div className="header-search">
                                                <input placeholder="Поиск" className="dropdown-button" className="search-input" type="text" />
                                                <span className="search"><img onClick={() => this.setState({ showSearch: 1 })} src="/images/buttons/search.svg" alt="" /></span>
                                            </div>
                                            <div className="bg-black results-modal" data-uk-dropdown="pos: bottom-justify;mode:click">
                                                <ul className="results-modal-body">
                                                    <div className="results-modal-item">
                                                        <div className="results-modal-img">
                                                            <img src="https://image.tmdb.org/t/p/original/ziEuG1essDuWuC5lpWUaw1uXY2O.jpg" alt="" />
                                                        </div>
                                                        <div className="results-modal-info">
                                                            <div className="results-modal-name">Джон Уик 3</div>
                                                            <div className="results-modal-about">2002 • Приключения • Казахстан</div>
                                                        </div>
                                                    </div>
                                                    <div className="results-modal-item">
                                                        <div className="results-modal-img">
                                                            <img src="https://image.tmdb.org/t/p/original/ziEuG1essDuWuC5lpWUaw1uXY2O.jpg" alt="" />
                                                        </div>
                                                        <div className="results-modal-info">
                                                            <div className="results-modal-name">Джон Уик 3</div>
                                                            <div className="results-modal-about">2002 • Приключения • Казахстан</div>
                                                        </div>
                                                    </div>
                                                    <div className="results-modal-item">
                                                        <div className="results-modal-img">
                                                            <img src="https://image.tmdb.org/t/p/original/ziEuG1essDuWuC5lpWUaw1uXY2O.jpg" alt="" />
                                                        </div>
                                                        <div className="results-modal-info">
                                                            <div className="results-modal-name">Джон Уик 3</div>
                                                            <div className="results-modal-about">2002 • Приключения • Казахстан</div>
                                                        </div>
                                                    </div>
                                                    <Link to="/search" className="results-modal-more">
                                                        Все результаты
                                                    </Link>
                                                </ul>
                                            </div>
                                        </Fragment>
                                }


                                {this.state.showSearch === 1 ? <span className="search"><img onClick={() => this.setState({ showSearch: 2 })} src="/images/buttons/search.svg" alt="" /></span> : null}

                                {/* <span className="sign-in">ВОЙТИ</span> */}
                                <span className="profile" onClick={this.openSign}>НАРИМАН ДУЙСЕКОВ</span>

                            </div>
                            <div className="menu-mob">
                                {this.state.showSearch === 1 ? null : <input placeholder="Поиск" className="search-input" type="text" />}
                                {this.state.showSearch === 1 ? <img src="images/buttons/search.svg" alt="" onClick={() => this.setState({ showSearch: 2 })} /> : <img className="menu-mob-search" src="images/buttons/search.svg" alt="" onClick={() => this.setState({ showSearch: 1 })} />}

                                <img src="images/buttons/menu.svg" alt="" onClick={this.openSideBar} data-uk-toggle="target: #offcanvas-slide" />
                                <img src="images/buttons/menu-sign.svg" alt="" className="menu-mob-sign" onClick={this.openSign} />

                                <div id="offcanvas-slide" uk-offcanvas="overlay: true;flip:true">
                                    <div className={`uk-offcanvas-bar menu-sidebar ${this.state.sideBarActive}`}>
                                        <div className="menu-sidebar-header">
                                            {this.state.menuSideBar === 2 ? <div onClick={() => this.changeSideBar(1)} className="menu-sidebar-back"><img src="images/buttons/back.svg" alt="" /></div> : null}
                                            {this.state.menuSideBar === 3 ? <div onClick={() => this.changeSideBar(1)} className="menu-sidebar-back"><img src="images/buttons/back.svg" alt="" /></div> : null}
                                            {this.state.menuSideBar === 4 ? <div onClick={() => this.changeSideBar(2)} className="menu-sidebar-back"><img src="images/buttons/back.svg" alt="" /></div> : null}
                                            <div className="uk-offcanvas-close menu-sidebar-close" type="button" onClick={() => this.changeSideBar(1)}>
                                                <img src="images/buttons/exit.svg" alt="" />
                                            </div>
                                        </div>
                                        {this.state.menuSideBar === 3 ? null : <div className="menu-sidebar-profile" onClick={() => this.changeSideBar(2)}>
                                            <div className="menu-sidebar-avatar">
                                                <img src="images/persons/mamoa.jpeg" alt="" />
                                            </div>
                                            <div className="menu-sidebar-profile-body">
                                                <div className="menu-sidebar-profile-name">Нариман Дуйсеков</div>
                                                <div className="menu-sidebar-profile-contacts">+7 700 200 50 10</div>
                                                <div className="menu-sidebar-profile-contacts">nariman.duisekov@gmail.com</div>

                                            </div>
                                        </div>}
                                        {this.state.menuSideBar === 1 ? <div className="menu-m-items">
                                            {this.props.location.pathname === '/' ? <div onClick={(e) => this.closeSideBar(e, '/')} className="menu-m-item menu-item-active uk-offcanvas-close"><img src="images/buttons/menu-main.svg" alt="" />Главная</div> : <div onClick={(e) => this.closeSideBar(e, '/')} className="menu-m-item uk-offcanvas-close"><img src="images/buttons/menu-main.svg" alt="" />Главная</div>}
                                            {this.props.location.pathname === '/my' ? <div onClick={(e) => this.closeSideBar(e, '/my')} className="menu-m-item menu-item-active uk-offcanvas-close"><img src="images/buttons/menu-my.svg" alt="" />Мои фильмы</div> : <div onClick={(e) => this.closeSideBar(e, '/my')} className="menu-m-item uk-offcanvas-close"><img src="images/buttons/menu-my.svg" alt="" />Мои фильмы</div>}
                                            {this.props.location.pathname === '/favorites' ? <div onClick={(e) => this.closeSideBar(e, '/favorites')} className="menu-m-item menu-item-active uk-offcanvas-close"><img src="images/buttons/menu-favorite.svg" alt="" />Отмеченные</div> : <div onClick={(e) => this.closeSideBar(e, '/favorites')} className="menu-m-item uk-offcanvas-close"><img src="images/buttons/menu-favorite.svg" alt="" />Отмеченные</div>}
                                            <div onClick={() => this.changeSideBar(3)} className="menu-m-item"><img src="images/buttons/menu-favorite.svg" alt="" />Подборка жанров</div>
                                        </div> : null}

                                        {this.state.menuSideBar === 2 ? <div className="menu-m-profile">
                                            <div className="menu-m-profile-item">
                                                <div className="menu-m-profile-label">Почта</div>
                                                <div className="menu-m-profile-text">nariman.duisekov@gmail.com</div>
                                            </div>
                                            <div className="menu-m-profile-item">
                                                <div className="menu-m-profile-label">Номер телефона</div>
                                                <div className="menu-m-profile-text">+77002005010</div>
                                            </div>
                                            <div className="sign-button" onClick={() => this.changeSideBar(4)}>
                                                Изменить
                                            </div>
                                            <div className="menu-m-profile-exit">
                                                Выйти
                                            </div>
                                        </div> : null}

                                        {this.state.menuSideBar === 3 ? <div className="sign-genres">
                                            <div className="sign-title">Подборка для Вас</div>
                                            <div className="sign-subtitle">Выберите, как минимум, три жанра.</div>
                                            {
                                                this.state.genres.map(this.renderGenre)
                                            }

                                            <div className="sign-button">
                                                Сохранить
                                            </div>
                                        </div> : null}

                                        {this.state.menuSideBar === 4 ? <div className="menu-m-profile">
                                            <TextField value="Нариман"
                                                text="Изменить имя" width="100%"
                                            />
                                            <TextField value="Дуйсеков"
                                                text="Изменить фамилию" width="100%"
                                            />
                                            <TextField value="+7747744777"
                                                text="Изменить телефон" width="100%"
                                            />
                                            <TextField value="nariman.duisekov@gmail.com"
                                                text="Изменить почту" width="100%"
                                            />
                                            <div className="sign-button">
                                                Сохранить
                                            </div>
                                        </div> : null}
                                    </div>
                                </div>

                                {/* {this.state.showMenuMob === 2 ? <div className="menu-mob-modal">
                                    <div className="menu-m-top"><img onClick={(e) => this.closeSideBar(e, '/')} src="/images/buttons/exit.svg" alt="" /></div>
                                    <div className="menu-m-items">
                                        {this.props.location.pathname === '/' ? <div onClick={(e) => this.closeSideBar(e, '/')} className="menu-m-item menu-item-active">Главная</div> : <div onClick={(e) => this.closeSideBar(e, '/')} className="menu-m-item">Главная</div>}
                                        {this.props.location.pathname === '/my' ? <div onClick={(e) => this.closeSideBar(e, '/my')} className="menu-m-item menu-item-active">Мои фильмы</div> : <div onClick={(e) => this.closeSideBar(e, '/my')} className="menu-m-item">Мои фильмы</div>}
                                        {this.props.location.pathname === '/favorites' ? <div onClick={(e) => this.closeSideBar(e, '/favorites')} className="menu-m-item menu-item-active">Отмеченные</div> : <div onClick={(e) => this.closeSideBar(e, '/favorites')} className="menu-m-item">Отмеченные</div>}
                                    </div>
                                </div> : null} */}
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.show ?
                    <div className={`sign`}>
                        <div className="sign-close">
                            <img src="images/buttons/exit.svg" onClick={this.closeSign} />
                        </div>
                        <div className="sign-bg" onClick={this.closeSign}></div>
                        <Sign />
                    </div> : null}
                {this.state.showProfile ?
                    <div className="user">
                        <div className="sign-bg" onClick={this.closeProfile}></div>
                        <Profile />
                    </div>
                    : null}

                {/* {
                    this.state.toSearch.length > 0 ?
                        <Results value={this.state.toSearch} /> : null
                } */}

            </Fragment>
        )
    }
}