import React, { Fragment, Component } from 'react'
import { Link } from 'react-router-dom'

import axios from 'axios';

import "./purchased.css"
// import films from "../main/films.json"
import MyFilmItem from "../../components/my-film-item"


export default class Favourite extends Component {
    constructor(props) {
        super(props);
        this.state = {
            films: []
        };
    }

    componentWillMount() {
        axios.get('https://api.themoviedb.org/3/movie/top_rated?api_key=d09aed91f13503607360b4e7fcbf8140&language=ru&include_image_language=ru,null')
            .then((response) => {
                this.setState({ films: response.data.results })
                // console.log(response)
                // console.log(this.state.films)
            })
            .catch((err) => {
                console.log(err.response)
            })
    }

    componentDidMount() {
        window.scrollTo(0, 0)
    }

    render() {
        return (
            <Fragment>
                <div className="purchased">
                    <div className="p-main">
                        <div className="container">
                            <div className="page-title purchased-title">Мои фильмы</div>
                            <div className="p-body">
                                {
                                    this.state.films.map(function (film) {
                                        return <MyFilmItem key={film.id} film={film} />
                                    })
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        )
    }
}

