import React, { Fragment, Component } from 'react'
import { Link } from 'react-router-dom'

import axios from 'axios';

import "./person.css"
// import films from "../main/recomend.json"
import FilmItem from "../../components/film-item"


class Favourite extends Component {
    constructor(props) {
        super(props);
        this.state = {
            cast_id: props.match.params.id,
            cast: [],
            movies: [],
            images: [],
            open: 1,
            showImages: 1
        };
    }
    componentWillMount() {
        axios.get('https://api.themoviedb.org/3/person/' + this.state.cast_id + '?api_key=d09aed91f13503607360b4e7fcbf8140')
            .then((response) => {
                this.setState({
                    ...this.state,
                    cast: response.data
                })
            })
            .catch((err) => {
                console.log(err.response)
            })

        axios.get('https://api.themoviedb.org/3/person/' + this.state.cast_id + '/movie_credits?api_key=d09aed91f13503607360b4e7fcbf8140')
            .then((response) => {
                this.setState({
                    ...this.state,
                    movies: response.data.cast
                })
            })
            .catch((err) => {
                console.log(err.response)
            })

        axios.get('https://api.themoviedb.org/3/person/' + this.state.cast_id + '/images?api_key=d09aed91f13503607360b4e7fcbf8140')
            .then((response) => {
                this.setState({
                    ...this.state,
                    images: response.data.profiles
                })
            })
            .catch((err) => {
                console.log(err.response)
            })
    }

    componentDidMount() {
        window.scrollTo(0, 0);
        this.setState({ open: 2 })
    }

    render() {
        // console.log(this.state.cast)
        // console.log(this.state.movies)
        // console.log(this.state.images)
        return (
            this.state.open === 1 ?
                <div>...Loading</div> :
                <Fragment>
                    <div className="cast">
                        <div className="container">
                            <div className="cast-main">
                                <div className="cast-img">
                                    <img src={`https://image.tmdb.org/t/p/w780${this.state.cast.profile_path}`} alt="" />
                                </div>
                                <div className="cast-info">
                                    <div className="cast-name">{this.state.cast.name}</div>
                                    <div className="cast-bio"><img className="cast-img-mob" src={`https://image.tmdb.org/t/p/w780${this.state.cast.profile_path}`} alt=""/>{this.state.cast.biography}</div>
                                    <div className="cast-type">
                                        <div className="cast-type-name">Известность</div>
                                        <div className="cast-type-info">{this.state.cast.known_for_department}</div>
                                    </div>
                                    <div className="cast-type">
                                        <div className="cast-type-name">Дата рождения</div>
                                        <div className="cast-type-info">{this.state.cast.birthday}</div>
                                    </div>
                                    <div className="cast-type">
                                        <div className="cast-type-name">Место рождения</div>
                                        <div className="cast-type-info">{this.state.cast.place_of_birth}</div>
                                    </div>
                                </div>
                            </div>
                            <div className="cast-nav">
                                <div className="cast-nav-items">
                                    {this.state.showImages === 1 ? <div className="cast-nav-item cast-nav-active" onClick={() => this.setState({ showImages: 1 })}>Фильмография</div> : <div className="cast-nav-item" onClick={() => this.setState({ showImages: 1 })}>Фильмография</div>}
                                    {this.state.showImages === 2 ? <div className="cast-nav-item cast-nav-active" onClick={() => this.setState({ showImages: 2 })}>Фотогалерея</div> : <div className="cast-nav-item" onClick={() => this.setState({ showImages: 2 })}>Фотогалерея</div>}
                                </div>
                            </div>
                            <div className="cast-movies">
                                {this.state.showImages === 1 ?
                                    <div className="cast-movies">
                                        {
                                            this.state.movies.map(function (film) {
                                                return <FilmItem key={film.id} film={film} />
                                            })
                                        }
                                    </div> : <div className="cast-images">
                                        {
                                            this.state.images.map(function (img, key) {
                                                return <div key={key} className="cast-image">
                                                    <img  src={`https://image.tmdb.org/t/p/w780${img.file_path}`} alt="" />
                                                </div>
                                            })
                                        }
                                    </div>
                                }
                            </div>
                        </div>
                    </div>
                </Fragment>
        )
    }
}

export default Favourite
