import React, { Fragment, Component } from 'react'
import { Link } from 'react-router-dom'

import axios from "axios"

import $ from 'jquery';

import FilmItem from '../../components/film-item'
import Carousel from '../../components/carousel'
import './landing.css';
// import films from './films.json'
// import recomend from './recomend.json'

export default class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {
      films: [],
      recomend: [],
      active: 1,
      friends: [
        { id: 1, name: 'Нариман Дуйсеков', films: ['Брат или брак', 'Брат или брак 2', 'Бизнес по-казахски'], img: '/images/persons/mark.jpg' },
        { id: 2, name: 'Еламан Мырзаханов', films: ['Рэкетир', 'Бизнесмены', 'Районы', 'Тараз'], img: '/images/persons/robert.jpg' },
        { id: 3, name: 'Нариман Дуйсеков', films: ['Брат или брак', 'Брат или брак 2', 'Бизнес по-казахски'], img: '/images/persons/mark.jpg' },
        { id: 4, name: 'Еламан Мырзаханов', films: ['Рэкетир', 'Бизнесмены', 'Районы', 'Тараз'], img: '/images/persons/robert.jpg' },
        { id: 5, name: 'Нариман Дуйсеков', films: ['Брат или брак', 'Брат или брак 2', 'Бизнес по-казахски'], img: '/images/persons/mark.jpg' },
        { id: 6, name: 'Нариман Дуйсеков', films: ['Брат или брак', 'Брат или брак 2', 'Бизнес по-казахски'], img: '/images/persons/mark.jpg' },
        { id: 7, name: 'Нариман Дуйсеков', films: ['Брат или брак', 'Брат или брак 2', 'Бизнес по-казахски'], img: '/images/persons/mark.jpg' },
        { id: 8, name: 'Нариман Дуйсеков', films: ['Брат или брак', 'Брат или брак 2', 'Бизнес по-казахски'], img: '/images/persons/mark.jpg' },
        // { id: 9, name: 'Нариман Дуйсеков', films: ['Брат или брак', 'Брат или брак 2', 'Бизнес по-казахски'], img: '/images/persons/mark.jpg' },
        // { id: 10, name: 'Нариман Дуйсеков', films: ['Брат или брак', 'Брат или брак 2', 'Бизнес по-казахски'], img: '/images/persons/mark.jpg' },
        // { id: 8, name: 'Нариман Дуйсеков', films: ['Брат или брак', 'Брат или брак 2', 'Бизнес по-казахски'], img: '/images/persons/mark.jpg' },
        // { id: 8, name: 'Нариман Дуйсеков', films: ['Брат или брак', 'Брат или брак 2', 'Бизнес по-казахски'], img: '/images/persons/mark.jpg' },
        // { id: 8, name: 'Нариман Дуйсеков', films: ['Брат или брак', 'Брат или брак 2', 'Бизнес по-казахски'], img: '/images/persons/mark.jpg' },
      ]
    };
  }

  componentWillMount() {

    axios.get('https://api.themoviedb.org/3/movie/now_playing?api_key=d09aed91f13503607360b4e7fcbf8140&language=ru&include_image_language=ru,null')
      .then((response) => {
        this.setState({ films: response.data.results })
      })
      .catch((err) => {
        console.log(err.response)
      })

    axios.get('https://api.themoviedb.org/3/movie/popular?api_key=d09aed91f13503607360b4e7fcbf8140&language=ru&include_image_language=ru,null')
      .then((response) => {
        this.setState({ recomend: response.data.results })
      })
      .catch((err) => {
        console.log(err.response)
      })
  }

  componentDidMount() {
    window.scrollTo(0, 0);
  }

  render() {
    return (
      <Fragment>
        <div className="landing">
          <div className="main">

            <div className="main-title">
              <div className="container">
                {/* <div className="m-title">Казахстанский онлайн-кинотеатр</div> */}
                {/* <div className="m-subtitle">Добро пожаловать в Oiboi!</div> */}
              </div>
            </div>



            <Carousel title="Новинки" items={this.state.films} />

            <Carousel title="Популярное" items={this.state.recomend} />


            <div className="films">
              <div className="films-recomended">
                <div className="container"><div className="similar-title f-list-title">Друзья</div></div>
                <div className="s-slider">
                  {Object.keys(this.state.friends).length < 11 ?
                    <div className="f-list s-list" data-tabindex="-1" data-uk-slider="finite:true">
                      <a className="s-arrow-left" data-uk-slidenav-previous data-uk-slider-item="previous"><img src="images/buttons/arrow-left.svg" alt="" /></a>
                      <a className="s-arrow-right" data-uk-slidenav-next data-uk-slider-item="next"><img src="images/buttons/arrow-right.svg" alt="" /></a>
                      <ul className="uk-slider-items f-items s-s-items">
                        {
                          this.state.friends.map(function (friend, key) {
                            return <Link to={{
                              pathname: '/friend'
                            }} className="f-item f-item-friend" key={key}>
                              <div className="f-img">
                                <div style={{backgroundImage: `url(${friend.img})` }}></div>
                              </div>
                              <div className="film-name">{friend.name}</div>
                              <div className="film-info">{
                                friend.films.map(function (film) {
                                  return <span key={film}>{film},</span>
                                })
                              }</div>
                            </Link>
                          })
                        }
                      </ul>
                    </div> :
                    <div className="f-list s-list" data-tabindex="-1" data-uk-slider="center:true">
                      <a className="s-arrow-left" data-uk-slidenav-previous data-uk-slider-item="previous"><img src="images/buttons/arrow-left.svg" alt="" /></a>
                      <a className="s-arrow-right" data-uk-slidenav-next data-uk-slider-item="next"><img src="images/buttons/arrow-right.svg" alt="" /></a>
                      <ul className="uk-slider-items f-items">
                        {
                          this.state.friends.map(function (friend, key) {
                            return <Link to={{
                              pathname: '/friend'
                            }} className="f-item " key={key}>
                              <div className="f-img" style={{backgroundImage:friend.img}}>
                              </div>
                              <div className="film-name">{friend.name}</div>
                              <div className="film-info">{
                                friend.films.map(function (film) {
                                  return <span key={film}>{film},</span>
                                })
                              }</div>
                            </Link>
                          })
                        }
                      </ul>
                    </div>}
                </div>
              </div>
            </div>

            <div className="s-films">
              <div className="container">
                <div className="s-menu uk-child-width-1-3">

                  {this.state.active === 0 ? <span className="s-menu-item s-menu-active" onClick={() => this.setState({ active: 0 })}>Для вас</span> : <span className="s-menu-item" onClick={() => this.setState({ active: 0 })}>Для вас</span>}

                  {this.state.active === 1 ? <span className="s-menu-item s-menu-active" onClick={() => this.setState({ active: 1 })}>Популярное</span> : <span className="s-menu-item" onClick={() => this.setState({ active: 1 })}>Популярное</span>}

                  {this.state.active === 2 ? <span className="s-menu-item s-menu-active" onClick={() => this.setState({ active: 2 })}>Друзья</span> : <span className="s-menu-item" onClick={() => this.setState({ active: 2 })}>Друзья</span>}

                </div>

                <div className="s-films-list">

                  <ul className="uk-child-width-1-3 uk-child-width-1-4@s s-films-items" data-uk-grid>

                    {
                      this.state.active === 0 ? this.state.films.map(function (film) {
                        return <FilmItem key={film.id} film={film} />
                      }) : null
                    }
                    {
                      this.state.active === 1 ? this.state.recomend.map(function (film) {
                        return <FilmItem key={film.id} film={film} />
                      }) : null
                    }
                    {
                      <Fragment>
                        {this.state.active === 2 ? <div className="m-friend-title m-friend-title-mob page-title">Покупки друзей</div> : null}
                        {
                          this.state.active === 2 ? this.state.friends.map(function (friend) {
                            return <Link to="/friend" key={friend.id} className="friends-item">
                              <span className="friends-img">
                                <img src="/images/persons/mamoa.jpeg" alt="" />
                              </span>
                              <span className="friends-info">
                                <div className="friends-name">{friend.name}</div>
                                <div className="friends-films">
                                  {
                                    friend.films.map(function (film) {
                                      return <span key={film}>{film},</span>
                                    })
                                  }
                                </div>
                              </span>
                            </Link>
                          }) : null
                        }
                      </Fragment>


                    }

                  </ul>

                </div>
              </div>
            </div>





            <div className="main-footer">
              <div className="m-f-title">Весь контент на нашем сервисе легальный</div>
              <div className="m-f-subtitle">Войдите, чтобы смотреть фильмы</div>
              <div className="m-f-button"><button>Войти</button></div>

            </div>
          </div>



        </div>
      </Fragment>
    )
  }
}

