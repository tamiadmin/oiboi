import React, { Fragment, Component } from 'react'
import { Link } from 'react-router-dom'
import TextField from '../../components/input';

import './sign.css'

export default class Sign extends Component {
    constructor(props) {
        super(props);
        this.state = {
            show: this.props.show,
            count: 1,
            genres: ['Комедии', 'Семейные', 'Триллеры', 'Детские', 'Мультфильмы', 'Военные', 'Детективы', 'Боевики',
                'Криминальные', 'Мелодрамы', 'Сериалы', 'Ужасы', 'Приключения', 'Исторические', 'Фантастика', 'Биография', 'Мюзиклы', 'Драмы'],
            selected: {},
        };

        this.selectGenre = this.selectGenre.bind(this);
        this.renderGenre = this.renderGenre.bind(this);
    }

    changeStep(type) {
        this.setState(prevState => {
            return { count: type == 'add' ? prevState.count + 1 : prevState.count - 1 }
        });
    }

    selectGenre(el) {
        var selected = this.state.selected;
        selected[el] = !selected[el];
        this.setState({ selected: selected });
    }

    renderGenre(el) {
        var className = this.state.selected[el] ? 'sign-genre-selected' : '';
        var onClick = this.selectGenre.bind(this, el);
        return <span className={`sign-genre ${className}`} onClick={onClick} key={el}>{el}</span>;
    }

    render() {
        return (
            this.state.count < 3 ?
                <Fragment>
                    <div className="sign-body">
                        <div className="sign-title">Добро пожаловать</div>
                        <TextField placeholder="+7(747)303-97-58"
                            text="Номер телефона" width="100%"
                        />
                        {this.state.count > 1 ? <TextField placeholder="••••••••"
                            text="Код подтверждения" width="100%"
                        /> : null}
                        <div className="sign-button" onClick={this.changeStep.bind(this, 'add')}>
                            Далее
                            </div>
                        <div className="sign-small">Войдите, чтобы смотреть фильмы</div>
                    </div>
                </Fragment>
                : <Fragment>
                    {this.state.count == 3 ? <div className="sign-body">
                        <div className="sign-title">Регистрация</div>
                        <div className="sign-divider">
                            <TextField placeholder="Нариман"
                                text="Имя" width="48%"
                            />
                            <TextField placeholder="Дуйсеков"
                                text="Фамилия" width="48%"
                            />
                        </div>


                        <TextField placeholder="nariman.duisekov@gmail.com"
                            text="Email" width="100%"
                        />
                        <TextField placeholder="••••••••"
                            text="Пароль" width="100%"
                        />
                        <div className="sign-button" onClick={this.changeStep.bind(this, 'add')}>
                            Далее
                            </div>
                        <div className="sign-small">Зарегистрируйтесь, чтобы смотреть фильмы</div>
                    </div> : <div className="sign-body">
                            <div className="sign-title">Подборка для Вас</div>
                            <div className="sign-subtitle">Выберите, как минимум, три жанра.</div>
                            <div className="sign-genres">
                                {
                                    this.state.genres.map(this.renderGenre)
                                }
                            </div>
                            {Object.keys(this.state.selected).length < 3 ? <div className="sign-button sign-button-dasable">
                                Зарегистрироваться
                                </div> : <div className="sign-button" onClick={this.changeStep.bind(this, 'add')}>
                                    Зарегистрироваться
                                </div>}

                            <div className="sign-small">Зарегистрируйтесь, чтобы смотреть фильмы</div>
                        </div>
                    }

                </Fragment>
        )
    }
}