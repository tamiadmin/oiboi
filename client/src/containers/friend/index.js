import React, { Fragment, Component } from 'react'
import { Link } from 'react-router-dom'

import axios from "axios"

import './friend.css'

import Carousel from "../../components/carousel"

export default class Friend extends Component {
  constructor(props) {
    super(props);
    this.state = {
      films: [],
      recomend: [],
      active: 1
    };
  }

  componentWillMount() {
    axios.get('https://api.themoviedb.org/3/movie/top_rated?api_key=d09aed91f13503607360b4e7fcbf8140&language=ru&include_image_language=ru,null')
      .then((response) => {
        this.setState({ films: response.data.results })
      })
      .catch((err) => {
        console.log(err.response)
      })

    axios.get('https://api.themoviedb.org/3/movie/popular?api_key=d09aed91f13503607360b4e7fcbf8140&language=ru&include_image_language=ru,null')
      .then((response) => {
        this.setState({ recomend: response.data.results })
      })
      .catch((err) => {
        console.log(err.response)
      })
  }

  componentDidMount() {
    window.scrollTo(0, 0)
  }

  render() {
    return (
      <Fragment>
        <div className="friend">
          <div className="main">
            <div className="friend-title">
              Нариман Дуйсеков
            </div>
            <div className="friend-purchased">
              <div className="container">
                <div className="friend-name">
                  Покупки
                </div>
              </div>
              <div className="f-p-slider">
                {
                  this.state.films.map(function (film) {
                    return <img key={film.id} src={`https://image.tmdb.org/t/p/w780${film.poster_path}`} alt="" />
                  })
                }
              </div>
            </div>
            <div className="friend-recomend">
              <div className="container">
                <div className="friend-name">
                  Рекомендует
                </div>
              </div>
              <div className="f-p-slider">
                {
                  this.state.recomend.map(function (film) {
                    return <img key={film.id} src={`https://image.tmdb.org/t/p/w780${film.poster_path}`} alt="" />
                  })
                }
              </div>
            </div>

            <Carousel title="Покупки" items={this.state.films} />

            <Carousel title="Рекомендует" items={this.state.recomend} />

          </div>
        </div>
      </Fragment>
    )
  }
}

