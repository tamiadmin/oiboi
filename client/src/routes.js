import React, { Fragment } from 'react'
import { Switch, Route } from 'react-router-dom'


import Main from './containers/main'
import Purchased from './containers/purchased'
import Favourite from './containers/favourite'
import Friends from './containers/friends'
import Friend from './containers/friend'
import Results from './containers/results'
import Person from './containers/person'
import Film from './containers/film'
import Header from './containers/header'
import Footer from './components/footer'
import Slider from './components/slider'
import Agreement from './components/agreement'
import Help from './components/help'
import Ads from './components/ads'
import About from './components/about'
import Contact from './components/contact'
// import SmallSlider from './components/small-slider'


const Routes = () =>

  <Fragment>
    <Route component={Header} />

    {["/", "/friends"].map((path, index) =>
      <Route exact path={path} component={Slider} key={index} />
    )}

    <Route exact path="/" component={Main} />
    <Switch>
      <Route path="/search" component={Results} />
      <Route path="/my" component={Purchased} />
      <Route path="/friends" component={Friends} />
      <Route path="/friend" component={Friend} />
      <Route path="/favorites" component={Favourite} />
      <Route path="/person/:id" component={Person} />
      <Route path="/film/:id" component={Film} />
      <Route path="/agreement" component={Agreement} />
      <Route path="/help" component={Help} />
      <Route path="/ads" component={Ads} />
      <Route path="/about" component={About} />
      <Route path="/contact" component={Contact} />
    </Switch>
    <Footer />
  </Fragment>

export default Routes
