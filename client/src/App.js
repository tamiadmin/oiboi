import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter, HashRouter } from 'react-router-dom'

import Routes from './routes'

class App extends Component {

  render() {
    return (
      <HashRouter>
        <Routes />
      </HashRouter>
    )
  }
}


export default App