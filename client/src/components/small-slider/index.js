import React from 'react'
import { Link } from 'react-router-dom'

const smallSlider = () =>
    <div className="p-s-slider">
        <div className="f-list s-list" data-tabindex="-1" data-uk-slider="center:true;">
            {/* <a className="s-arrow-left" data-uk-slidenav-previous data-uk-slider-item="previous"><img src="images/buttons/arrow-left.svg" alt="" /></a> */}
            {/* <a className="s-arrow-right" data-uk-slidenav-next data-uk-slider-item="next"><img src="images/buttons/arrow-right.svg" alt="" /></a> */}
            <ul className="uk-slider-items f-items p-s-items">
                <li>
                    <img src="https://image.tmdb.org/t/p/original/3P52oz9HPQWxcwHOwxtyrVV1LKi.jpg" alt="" data-uk-cover />
                </li>
                <li>
                    <img src="https://image.tmdb.org/t/p/original/bOGkgRGdhrBYJSLpXaxhXVstddV.jpg" alt="" data-uk-cover />
                </li>
                <li>
                    <img src="https://image.tmdb.org/t/p/original/nGsNruW3W27V6r4gkyc3iiEGsKR.jpg" alt="" data-uk-cover />
                </li>
            </ul>
        </div>
    </div>;

export default smallSlider

