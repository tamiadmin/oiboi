import React, { Fragment, Component } from 'react'

import { Link } from 'react-router-dom'

const footer = ({ film }) =>
    <Fragment>
        <div className="footer-line"></div>
        <div className="footer">
            <div className="container">
                <div className="footer-body">
                    <div className="socials">
                        <a target="_blank" href="https://ru-ru.facebook.com/pages/category/Public-Figure/Elon-Musk-19958149870/"><img className="socials-icons" src="/images/logos/facebook.svg" alt=""/></a>
                        <a target="_blank" href="https://vk.com/elonmusk"><img className="socials-icons" src="/images/logos/vk.svg" alt=""/></a>      
                        <a target="_blank" href="https://www.instagram.com/elonrmuskk/"><img className="socials-icons" src="/images/logos/instagram.svg" alt=""/></a>
                        <a target="_blank" href="https://t.me/elonmusk"><img className="socials-icons" src="/images/logos/telegram.svg" alt=""/></a>
                    </div>
                    <div className="footer-links">
                        <Link to="/agreement">Пользовательское соглашение</Link>
                        <Link to="/contact">Обратная связь</Link>
                        <Link to="/help">Помощь</Link>
                        <Link to="/ads">Реклама</Link>
                        <Link to="/about">О сервисе</Link>
                    </div>
                    <div className="application-links">
                        <a  target="_blank" href="https://play.google.com/store/apps/details?id=ru.ivi.client&hl=ru"><img src="/images/google-play.svg" alt=""/></a>
                        <a  target="_blank" href="https://itunes.apple.com/ru/app/ivi-фильмы-и-сериалы-онлайн/id455705533?mt=8"><img src="/images/app.svg" alt=""/></a>
                    </div>
                    <div className="license">
                        <a>QEL.mobi</a>&<a> MSLM Company</a>
                        <a>2019 OiBOi • Все права зашищены</a>
                    </div>
                </div>
            </div>
        </div>
    </Fragment>


export default footer