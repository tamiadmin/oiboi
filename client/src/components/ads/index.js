import React from 'react'
import { Link } from 'react-router-dom'

import './ads.css'

const Ads = (props) =>
    <div className="ads">
        <div className="container">
            <h1 className="page-title ads-title">Реклама</h1>
            <div className="ads-body">
                <div className="ads-image"><img src="https://s1.vcdn.biz/static/f/837957001/image.jpg" alt="" /></div>
                <div className="ads-info">
                    <div className="ads-info-text">
                        По вопросам размещения рекламы на MEGOGO в Казахстане
    обращайтесь в PC «ViDigital»:

    sales@vidigital.kz +7 (727) 334 06 20

    Отдел прямых продаж рекламы на MEGOGO:

    ads@megogo.net
                    </div>
                </div>
            </div>
        </div>
    </div>;

export default Ads

