import React from 'react'
import { Link } from 'react-router-dom'

import './film-item.css'

const filmItem = ({ film }) =>
    <Link to={{
        pathname: '/film/'+film.id,
        state: { film_id: film.id }
    }} className="f-item">
        <div className="f-img">
            <img src={`https://image.tmdb.org/t/p/original${film.poster_path}`} alt="" />
        </div>
        <div className="film-name">{film.title}</div>
        <div className="film-info">2002 • Приключения • Казахстан</div>
    </Link>;

export default filmItem