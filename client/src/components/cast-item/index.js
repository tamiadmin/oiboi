import React from 'react'
import { Link } from 'react-router-dom'

import './cast-item.css'

const castItem = ({ cast }) =>
    <Link to={{
        pathname: '/person/' + cast.id,
        state: { cast_id: cast.id }
    }} className="cast-item">
        {
            cast.profile_path === null ? <img src={'images/custom/actor.jpg'} alt="" /> :
                <img src={`https://image.tmdb.org/t/p/w500${cast.profile_path}`} alt="" />
        }
        <div className="cast-item-name">{cast.name}</div>
    </Link>;

export default castItem