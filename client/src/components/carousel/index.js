import React from 'react'
import { Link } from 'react-router-dom'

import FilmItem from '../film-item'

const Carousel = (props) =>
    <div className="films">
        <div className="container"><div className="similar-title f-list-title">{props.title}</div></div>
        <div className="s-slider">
            {Object.keys(props.items).length < 11 ?
                <div className="f-list s-list" data-tabindex="-1">
                    <a className="s-arrow-left" data-uk-slidenav-previous data-uk-slider-item="previous"><img src="images/buttons/arrow-left.svg" alt="" /></a>
                    <a className="s-arrow-right" data-uk-slidenav-next data-uk-slider-item="next"><img src="images/buttons/arrow-right.svg" alt="" /></a>
                    <ul className="uk-slider-items f-items s-s-items">
                        {
                            props.items.map(function (item, key) {
                                return <FilmItem key={key} film={item} />
                            })
                        }
                    </ul>
                </div> :
                <div className="f-list s-list" data-tabindex="-1" data-uk-slider="center:true">
                    <a className="s-arrow-left" data-uk-slidenav-previous data-uk-slider-item="previous"><img src="images/buttons/arrow-left.svg" alt="" /></a>
                    <a className="s-arrow-right" data-uk-slidenav-next data-uk-slider-item="next"><img src="images/buttons/arrow-right.svg" alt="" /></a>
                    <ul className="uk-slider-items f-items">
                        {
                            props.items.map(function (item, key) {
                                return <FilmItem key={key} film={item} />
                            })
                        }
                    </ul>
                </div>
            }

        </div>
    </div>;

export default Carousel

