import React, { Fragment, Component } from 'react'
import { Link } from 'react-router-dom'
import TextField from '../../components/input';
import MultilineTextField from '../../components/input/multiline-input';

import './contact.css';

export default class Sign extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    render() {
        return (
            <Fragment>
                <div className="contact">
                    <div className="container">
                        <h1 className="page-title contact-title">Обратная связь</h1>
                        <div className="contact-body">
                            <form className="contact-form">
                                <TextField placeholder=""
                                    text="Имя*" width="100%"
                                />
                                <TextField placeholder="+7(747)303-97-58"
                                    text="Телефон*" width="100%"
                                />
                                <TextField placeholder="help@oiboi.kz"
                                    text="Почта*" width="100%"
                                />
                                <TextField placeholder="Вопрос"
                                    text="Тема" width="100%"
                                />
                                <MultilineTextField placeholder=""
                                    text="Cообщение*" width="100%"
                                />
                                <div className="contact-button">
                                    Отправить
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </Fragment>
        )
    }
}