import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';

const styles = {
    root: {
        color: '#737475',
    },
    input: {
        color: "#fff",
    },
    margin: {
        marginBottom: "10px"
    },
    cssLabel: {
        '&$cssFocused': {
            color: '#1e90ff',
        },
        '&:after': {
            fontSize: "13px",

        },
        color: "#737475"
    },
    cssFocused: {},
    cssUnderline: {
        '&:before': {
            borderBottomColor: '#737475',
        },
        '&:after': {
            borderBottomColor: '#1e90ff',
        },
        '&$cssFocused': {
            borderBottomColor: '#1e90ff',
        },
    },
    cssOutlinedInput: {
        '&$cssFocused $notchedOutline': {
            borderColor: '#737475',
        },
    },
    notchedOutline: {},
};

function CustomizedInputs(props) {
    const { classes } = props;

    return (
        <FormControl className={classes.margin} style={{ width: props.width }}>
            <InputLabel
                htmlFor="custom-css-standard-input"
                classes={{
                    root: classes.cssLabel,
                    focused: classes.cssFocused,
                }}
            >
                {props.text}
            </InputLabel>
            <Input
                id="custom-css-standard-input"
                classes={{
                    underline: classes.cssUnderline,
                    color: classes.cssColor
                }}
                value={props.value}
                multiline
                className="sign-input"
                color="#fff"
                placeholder={props.placeholder}
            />
        </FormControl>
    );
}

CustomizedInputs.propTypes = {
    classes: PropTypes.object.isRequired
};

export default withStyles(styles)(CustomizedInputs);