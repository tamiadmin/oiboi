import React from 'react'
import { Link } from 'react-router-dom'

const Slider = () =>
    <div className="slider">
        <div className="uk-position-relative uk-visible-toggle uk-light" data-tabindex="-1" data-uk-slideshow="ratio: 32:9; animation: fade; autoplay: true; autoplay-interval:5000;min-height: 200;">
            <ul className="uk-slideshow-items">
                {/* <li>
                    <img src="https://image.tmdb.org/t/p/original/3P52oz9HPQWxcwHOwxtyrVV1LKi.jpg" alt="" data-uk-cover />
                </li>
                <li>
                    <img src="https://image.tmdb.org/t/p/original/bOGkgRGdhrBYJSLpXaxhXVstddV.jpg" alt="" data-uk-cover />
                </li> */}
                <li className="slider-i" style={{ background: 'url(' + "https://image.tmdb.org/t/p/original/nGsNruW3W27V6r4gkyc3iiEGsKR.jpg" + ')' }}>
                    <div className="slider-bg">
                        <div className="container">
                            <div className="slider-item">
                                <div className="slider-name">Лучшее предложение</div>
                                <div className="slider-subtitle">Set in the present, the series offers a bold, subversive take on Archie, Betty, Veronica and their friends, exploring the surreality of small-town life, the darkness and weirdness bubbling beneath...</div>
                                <div className="slider-trailer-button"> <img src="images/buttons/play-slider.svg" alt=""/> Смотреть</div>
                            </div>
                        </div>
                    </div>
                </li>
                <li className="slider-i" style={{ background: 'url(' + "https://image.tmdb.org/t/p/original/bOGkgRGdhrBYJSLpXaxhXVstddV.jpg" + ')' }}>
                    <div className="slider-bg">
                        <div className="container">
                            <div className="slider-item">
                                <div className="slider-name">Лучшее предложение</div>
                                <div className="slider-subtitle">Set in the present, the series offers a bold, subversive take on Archie, Betty, Veronica and their friends, exploring the surreality of small-town life, the darkness and weirdness bubbling beneath...</div>
                                <div className="slider-trailer-button"> <img src="images/buttons/play-slider.svg" alt=""/> Смотреть</div>
                            </div>
                        </div>
                    </div>
                </li>
                
            </ul>
            <a className="uk-position-center-left uk-position-small uk-hidden-hover" href="#" data-uk-slidenav-previous data-uk-slideshow-item="previous"></a>
            <a className="uk-position-center-right uk-position-small uk-hidden-hover" href="#" data-uk-slidenav-next data-uk-slideshow-item="next"></a>
        </div>
        <img src="/images/shadow.png" alt="" className="slider-shadow" />
    </div>;

export default Slider

