import React from 'react'
import { Link } from 'react-router-dom'

import './my-film-item.css'

const filmItem = ({ film }) =>
    <div className="m-f-item">
        <div className="m-f-img" style={{ backgroundImage: `url(https://image.tmdb.org/t/p/w370_and_h556_bestv2${film.poster_path})` }}>
            <span className="m-f-left">
                <div>Осталось 4 дн</div>
            </span>
            <img src="images/buttons/play-black.svg" alt="" />
        </div>
        <div className="m-f-about">
            <div>
                <div className="m-film-name">{film.title}</div>
                <div className="m-film-info">2002 • 142 мин • Казахстан</div>
                <div className="m-film-info">
                    Приключения
                </div>
                <div className="m-f-left-mob">
                    Осталось 4 дн
                </div>
            </div>
            {/* <div className="f-buttons m-f-buttons"> */}
            {/* <span ><img src="/images/buttons/age.svg" alt="" /></span> */}
            {/* <span className="m-f-download"><img src="/images/buttons/download.svg" alt="" /></span> */}
            {/* </div> */}
        </div>
        <div className="m-f-download-mob"><img src="/images/buttons/download.svg" alt="" /></div>

    </div>
    ;

export default filmItem