const express = require('express')
const bodyParser = require('body-parser')
const fs = require('fs')
const path = require('path')
const passport = require('passport')

const app = express()
const port = process.env.PORT || 8080;

app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));

app.use(passport.initialize());

// require('./config/passport')(passport);

app.use(express.static('client/public/'));
app.use('/images/', express.static('../client/images'))
app.use('/fonts/', express.static('../client/style/fonts'))


app.use(require('./routes/index.js'));



app.get('*', (req, res) => {
  res.sendFile('index.html', { root: path.join(__dirname, '../client/public') });
});

app.listen(port, () => {
    console.log(`SERVER RUNNNING ON PORT ${port}`)
});

module.exports = app
