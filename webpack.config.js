var path = require('path')

module.exports = {
  devtool: 'source-map',
  entry: [
    './client/src/index.js'
  ],
  output: {
    path: path.join(__dirname, '/client/public/build'),
    filename: 'bundle.js',
    publicPath: '/client/public/static/'
  },

  watch: true,

  module: {
    loaders: [
      {
        test: /\.jsx?$/,
        exclude:/(node_modules|bower_components)/,
        loader: 'babel-loader',
        query: {
          presets: ['es2015', 'react', "stage-0"]
        }
      },
      {
        test: /\.css/,
        loaders: ['style-loader', 'css-loader'],
      },
      {
        test: /\.woff2?$|\.ttf$|\.eot$|\.svg$|\.png|\.jpe?g|\.gif$/,
        loader: 'file-loader?name=[name].[ext]'
      }
    ]
  }
}
